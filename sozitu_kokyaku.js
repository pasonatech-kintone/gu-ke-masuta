/** User     :双日新新都市開発株式会社
 *  System   :オプション受注管理
 *  App      :顧客マスタ
 *  Function :画面制御
 *  Create   :2018.02.07 by fukui
 *  **********************************************************
 *  Update   :YYYY.MM.DD modifier 
 *  **********************************************************
 * 物件別建物アプリ GET 
 * マスタアプリ     GET
 * 顧客マスタ       GET
 */

//--------------------------------------
// 広域変数宣言
//--------------------------------------

jQuery.noConflict();
(function($) {
    "use strict";

	//------------------------------------------------------
    // 定数宣言
	//------------------------------------------------------
var dbg_flg = 1;
//var dbg_flg = 0;
	const EVENT_INDEX_SHOW = ['app.record.index.show'];
	const EVENT_DETAIL_SHOW = ['app.record.detail.show'];
	
	const EVENT_INDEX_EDIT_SHOW = ['app.record.index.edit.show'];
	const EVENT_CREATE_EDIT_SHOW = ['app.record.create.show','app.record.edit.show'];
	
	const EVENT_INDEX_EDIT_SUBMIT = ['app.record.index.edit.submit'];
	const EVENT_CREATE_EDIT_SUBMIT = ['app.record.create.submit','app.record.edit.submit'];

	const EVENT_INDEX_EDIT_SUCCESS = ['app.record.index.edit.submit.success'];
	const EVENT_CREATE_EDIT_SUCCESS = ['app.record.create.submit.success','app.record.edit.submit.success'];

	const EVENT_INDEX_DELETE_SUBMIT = ['app.record.index.delete.submit'];
	const EVENT_DETAIL_DELETE_SUBMIT = ['app.record.detail.delete.submit'];
	
	const EVENT_PROCESS_PROCEED = ['app.record.detail.process.proceed'];
	
	const EVENT_CHANGE_CONDO_CD = [
		'app.record.create.change.txt_condo_cd','app.record.edit.change.txt_condo_cd'
	];

	const EVENT_CHANGE_CHK_CANCEL = [
		'app.record.create.change.chk_cancel','app.record.edit.change.chk_cancel'
	];
	const EVENT_CHANGE_CHK_MOVE = [
		'app.record.create.change.chk_move','app.record.edit.change.chk_move'
	];


	const DEF_CHK_CANCEL = '解約';
    const DEF_MODE_CANCEL = 1;
    const DEF_MODE_MOVE = 2;
    
    const DEF_DRP_CANCEL = '顧客都合';
    const DEF_DRP_MOVE = '住戸移動';
    
    const DEF_CHK_ORDER_CANCEL = 'キャンセル';
    const DEF_DRP_ORDER_CANCEL = '住戸キャンセル';
    const DEF_DRP_ORDER_MOVE = '住戸移動';
    
    const DEF_CANCEL_NUMBER = '99-';

	//------------------------------------------------------
	// 外部変数宣言
	//------------------------------------------------------
	var ex_change_number_flg = 0; // 解約/住戸移動フラグ
	var ex_pre_room_number = ''; // 修正前部屋番号
	
  	//------------------------------------------------------
	// 関連アプリ更新
	//  function : updapp_customer
	//  @event   : 画面表示情報
	//  @appid   : 更新対象アプリID
	//  return   : promise
	//------------------------------------------------------
	function updapp_customer(event, appid){
		//--------------------------------------
		// 更新条件生成
		//--------------------------------------
		var txt_customer_id = event['record']['txt_customer_id']['value'];
		var str_query = '';
		
		var guest_flg = false;
		
		switch(appid){
		case kintone.app.getRelatedRecordsTargetAppId('rel_s_order'): // 協力会社様向け受注管理
			str_query = 'lup_customer_id=\"' + txt_customer_id + '\"';
			break;
		case kintone.app.getRelatedRecordsTargetAppId('rel_g_order'): // 業者様向け受注管理
			str_query = 'lup_customer_id=\"' + txt_customer_id + '\"';
			break;
		}

		//--------------------------------------
		// 関連アプリ情報取得
		//--------------------------------------
		var get_param = {
			app: appid,
			query: str_query,
			fields: ['$id', 'lup_customer_id'],
			totalCount: true,
			isGuest: guest_flg
		};
		
		return kintoneUtility.rest.getAllRecordsByQuery(get_param).then(function(get_resp) {
			// success
			
			// 更新情報生成
			var recs = get_resp.records;
			var recs_cnt = recs.length;
			var records = [];

			// 件数チェック
			if(recs_cnt === 0){
				return ;
			}
			
			for(var i = 0; i < recs_cnt; i++){
				var rec = recs[i];
				var wk_rec = {};
				var wk_buf = {};
				wk_rec['id'] = rec['$id']['value'];
				wk_buf['lup_customer_id'] = {value:rec['lup_customer_id']['value']}; 
				wk_rec['record'] = wk_buf;
				records.push(wk_rec);
			}
			
			var put_param = {
				app: appid,
				records: records,
				isGuest: guest_flg
			};
		
			//--------------------------------------
			// 関連アプリ情報更新
			//--------------------------------------
			kintoneUtility.rest.putAllRecords(put_param).then(function(put_resp) {
				// success
if(dbg_flg)console.log(put_resp);
			}).catch(function(put_error) {
				// error
				console.log(put_error);
			});
		}).catch(function(get_error) {
			// error
			console.log(get_error);
		});		
	}
	
	/*
	 * function : 物件別業者マスタ取得及び設定
	 * @event   : 画面情報
	 * return   : promise
	 */
	function get_itemlist(event){
		var record = event.record;
		var txt_condo_cd = record['txt_condo_cd']['value'];

		//--------------------------------------
		// 物件別業者マスタ取得
		//--------------------------------------
		var get_query = 'lup_condo_cd = \"' + txt_condo_cd + '\"';
		var get_fields = ['usr_dealer'];

		var get_body = {
			'app' : kintone.app.getRelatedRecordsTargetAppId('rel_itemlist_cd'), // 物件別業者マスタ
			'query' : get_query,
			'fileds' : get_fields
		};

		return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', get_body).then(function(get_resp) {
			var get_list = get_resp['records'];
			var get_len = get_list.length;
			record['usr_dealer']['value'].length = 0; // init
			for(var i = 0; i < get_len; i++){
			    var wk_code = {};
			    wk_code['code'] = get_list[i]['usr_dealer']['value'][0]['code'];
                record['usr_dealer']['value'].push(wk_code);
			}
		},function(get_error){
			console.log(get_error);
			alert('物件別業者マスタの取得に失敗しました。\n[' + get_error.message + ']');
		});
	}
	
	/*
	 * function : 部屋番号重複チェック
	 * @event   : 画面情報
	 */
	function chk_unique(event){
		var record = event.record;
		var txt_condo_cd = record['txt_condo_cd']['value'];
		var txt_build_cd = record['txt_build_cd']['value'];
		var txt_room_number = record['txt_room_number']['value'];
		var txt_customer_id = record['txt_customer_id']['value'];
		var e_flg = 0;
		
		
		if(event.type === 'app.record.edit.submit'){
            e_flg = 1;
		}
		
		//--------------------------------------
		// 部屋番号重複チェック
		//--------------------------------------
		var get_query = 'txt_room_number = \"' + txt_room_number + '\"';
		if(e_flg){
            get_query += ' and txt_customer_id != \"' + txt_customer_id + '\"';
		}
		get_query += ' and txt_condo_cd = \"' + txt_condo_cd + '\"';
		get_query += ' and txt_build_cd = \"' + txt_build_cd + '\"';
		var get_fields = ['txt_room_number'];

		var get_body = {
			'app' : kintone.app.getId(),
			'query' : get_query,
			'fileds' : get_fields
		};
			
		return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', get_body).then(function(get_resp) {
			var get_list = get_resp['records'];
			var get_len = get_list.length;
			if(get_len !== 0){
				event.error = '同一物件および建物に同じ部屋番号が登録されています。';
			}
		},function(get_error){
			console.log(get_error);
			alert('自アプリの取得に失敗しました。\n[' + get_error.message + ']');
		});
	}
	
	/*
	 * function : 解約/住戸移動時制御
	 * @event   : 画面情報
	 */
	function set_cancel_move(event){
        var record = event.record;
        var chk_cancel = record['chk_cancel']['value'];
        var chk_move = record['chk_move']['value'];
        var txt_condo_cd = record['txt_condo_cd']['value'];
        var txt_build_cd = record['txt_build_cd']['value'];
        var txt_room_number = record['txt_room_number']['value'];
        
        // チェック状態判定
        // 同時チェック：不可
        if(chk_cancel.length !== 0 && chk_move.length !== 0){
            event.error = '解約と住戸移動は同時に設定できません。'
            return ; // 終了
        }
        
        // どちらかのみチェック
        if(chk_cancel.length !== 0 || chk_move.length !== 0){
            // 住戸番号チェック
            var wk_header = String(txt_room_number).slice(0, DEF_CANCEL_NUMBER.length);
            // すでに解約/移動済の場合は終了
            if(wk_header == DEF_CANCEL_NUMBER){
                return ;
            }
            
            //--------------------------------------
            // 住戸番号採番
            //--------------------------------------
            var get_query = 'txt_room_number like \"' + DEF_CANCEL_NUMBER + '\"';
            get_query += ' and txt_condo_cd = \"' + txt_condo_cd + '\"';
            get_query += ' and txt_build_cd = \"' + txt_build_cd + '\"';
            get_query += ' order by txt_room_number';
            var get_fields = ['txt_room_number'];

            var get_body = {
                'app' : kintone.app.getId(),
                'query' : get_query,
                'fileds' : get_fields
            };
            
            return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', get_body).then(function(get_resp) {
                var get_list = get_resp['records'];
                var get_len = get_list.length;
                var wk_number = '';
                
                // 発行済解約住戸番号をチェック
                for(var i = 0; i < get_len; i++){
                    var wk_room_number = get_list[i]['txt_room_number']['value'];
                    var wk_arr = wk_room_number.split('-'); // 99-x-住戸番号
                    if(wk_arr[2]){
                        // 同一住戸番号で解約済住戸番号あり
                        if(txt_room_number === wk_arr[2]){
                            var wk_x = Number(wk_arr[1]);
                            wk_x++;
                            wk_number = DEF_CANCEL_NUMBER + wk_x + '-' +  txt_room_number;
                            break;
                        }
                    }
                }
                // 解約済住戸番号なし
                if(wk_number === ''){
                    wk_number = DEF_CANCEL_NUMBER + '1-' + txt_room_number;
                }
                
                // 新規部屋番号設定
                record['txt_room_number']['value'] = wk_number;
                ex_change_number_flg = 1; // 解約/住戸移動時フラグ設定
            },function(get_error){
                console.log(get_error);
                alert('自アプリの取得に失敗しました。\n[' + get_error.message + ']');
            });
        }
	}
	
	/*
	 * function : 解約理由選択時制御
	 * @event   : 画面情報
	 */
    function ctrl_cancel(event){
        var record = event.record;
        var chk_cancel = record['chk_cancel']['value'];
        
        // 解約チェック時
        if(chk_cancel.length !== 0){
            record['drp_cancel']['disabled'] = false; // 解約理由：選択可
            record['date_cancel']['value'] = moment().format('YYYY-MM-DD');
            record['mtxt_cancel_note']['disabled'] = false; // 解約理由：編集可
        }
        // 解約未チェック時
        else {
            record['drp_cancel']['value'] = ''; // 解約理由：クリア
            record['drp_cancel']['disabled'] = true; // 解約理由：選択不可
            record['date_cancel']['value'] = ''; // 解約日付：クリア
            record['date_cancel']['disabled'] = true; // 解約日付：編集不可
            record['mtxt_cancel_note']['value'] = ''; // 解約理由：クリア
            record['mtxt_cancel_note']['disabled'] = true; // 解約理由：編集不可
        }
    }
    
	/*
	 * function : 住戸移動理由選択時制御
	 * @event   : 画面情報
	 */
    function ctrl_move(event){
        var record = event.record;
        var chk_move = record['chk_move']['value'];
        
        // 住戸移動チェック時
        if(chk_move.length !== 0){
            record['date_move']['value'] = moment().format('YYYY-MM-DD');
            record['mtxt_move_note']['disabled'] = false; // 住戸移動理由：編集可
        }
        // 解約未チェック時
        else {
            record['date_move']['value'] = ''; // 住戸移動日付：クリア
            record['date_move']['disabled'] = true; // 住戸移動日付：編集不可
            record['mtxt_move_note']['value'] = ''; // 住戸移動理由：クリア
            record['mtxt_move_note']['disabled'] = true; // 住戸移動理由：編集不可
        }
    }
    
	/*
	 * function : 保存ボタン押下時必須チェック
	 * @event   : 画面情報
	 */
	function chk_required(event){
        var record = event.record;
        
        // タイプ
        if(!record['txt_room_type']['value']){
            record['txt_room_type']['error'] = 'タイプは必須項目です。';
        }
        // カラー
        if(!record['txt_room_color']['value']){
            record['txt_room_color']['error'] = 'カラーは必須項目です。';
        }
        // 間取
        if(!record['txt_room_layout']['value']){
            record['txt_room_layout']['error'] = '間取は必須項目です。';
        }
	}
	
	/*
	 * function : 住所コピーボタン制御
	 * @event   : 画面情報
	 */
	function copy_address(event){
	    var wk_buf = kintone.app.record.get();
        var get_rec = wk_buf['record'];
        get_rec['txt_contact_zip']['value'] = get_rec['txt_contractor_zip']['value'];
        get_rec['txt_contact_add1']['value'] = get_rec['txt_contractor_add1']['value'];
        get_rec['txt_contact_add2']['value'] = get_rec['txt_contractor_add2']['value'];
        get_rec['link_contact_tel']['value'] = get_rec['link_contractor_tel']['value'];
        get_rec['link_contact_fax']['value'] = get_rec['link_contractor_fax']['value'];

        kintone.app.record.set(wk_buf);
	}

	/* function  : 郵便番号→住所取得
	 * @event    : true/表示 false/非表示
	 * return    : void
	 */
	function set_address(event, zip_field){
		// 住所情報設定項目
		var btn_arr = {};
		// 契約者情報
		btn_arr['sp_contractor_zip'] = {
			'zip' : 'txt_contractor_zip',
			'add' : 'txt_contractor_add1'
		};
		// 連絡・送付先
		btn_arr['sp_contact_zip'] = {
			'zip' : 'txt_contact_zip',
			'add' : 'txt_contact_add1'
		};
		var rec = kintone.app.record.get();
		var fl_zip = btn_arr[zip_field]['zip'];
		var fl_add = btn_arr[zip_field]['add'];
		var zipcode = rec['record'][fl_zip]['value'];
			
		if (!zipcode || !zipcode.match(/^[0-9]{3}\-?[0-9]{4}$/)) {
			rec['record'][fl_zip].error = '7桁の半角数字で入力して下さい。';
			kintone.app.record.set(rec);
			return;
		}
		var endpoint = 'https://madefor.github.io/postal-code-api/api/v1';
		var code1 = zipcode.replace(/^([0-9]{3}).*/, "$1");
		var code2 = zipcode.replace(/.*([0-9]{4})$/, "$1");
		kintone.proxy(endpoint + '/' + code1 + '/' + code2 + '.json', 'GET', {}, {}).then(function (args) {
			//success
			/*  args[0] -> body(文字列)
			 *  args[1] -> status(数値)
			 *  args[2] -> headers(オブジェクト)
			 */
			if (args[1] == 200) {
				//success
				var resp = JSON.parse(args[0]);
//				rec.record.zipcode.value = code1 + '-' + code2;
				rec['record'][fl_zip].error = null;
				rec['record'][fl_add].value = resp.data[0].ja.prefecture + resp.data[0].ja.address1 + resp.data[0].ja.address2 + resp.data[0].ja.address3;
//				rec.record.company.value = resp.data[0].ja.address4;
				kintone.app.record.set(rec);
				return;
			} else {
				//error
				alert('郵便番号から住所の検索に失敗しました。');
				console.log(args[1], args[0], args[2]);
				return;
			}
		}, function (error) {
			//error
			alert('郵便番号から住所の検索に失敗しました。');
			console.log(error); //proxy APIのレスポンスボディ(文字列)を表示
			return;
		});
	}
	
	/* function : 入金関連コード取得集計処理
	 * @event   : (i)画面情報
	 * return   : promise
	 */
    function calc_receipt(event){
        var record = event.record;
        var txt_customer_id = record['txt_customer_id']['value'];
        
        if(!txt_customer_id){
            return ; // 終了
        }
        
        // query生成
        var app_id = kintone.app.getRelatedRecordsTargetAppId('rel_receipt');
        var get_query = 'lup_customer_id = \"' + txt_customer_id + '\"';
        var get_fields = ['num_receipt'];
        
        var get_body = {
            'app' : app_id,
            'query' : get_query,
            'fields' : get_fields
        };
        
        // 入金管理アプリ(GET)
        return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', get_body, function(get_resp) {
            var get_list = get_resp.records;
            var get_len = get_list.length;
            var wk_totalAmount = 0;
            // 入金総額を集計
            for(var i = 0; i < get_len; i++){
                var wk_receipt = get_list[i]['num_receipt']['value'];
                if(wk_receipt){
                    wk_totalAmount += Number(wk_receipt);
                }
            }
            
            // 画面表示
            var put_html = '';
            put_html += '<div>';
            put_html += '　　　　入金総額　　\\' + String(wk_totalAmount).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' );
            put_html += '</div>';
            var el_div = document.createElement('div');
            el_div.innerHTML = put_html;
            kintone.app.record.getSpaceElement('sp_totalAmount').appendChild(el_div);
        }, function(get_error) {
            console.log(get_error);
            alert('入金管理アプリの取得に失敗しました。\n[' + get_error.message + ']');
        });
    }
    
	/* function : 受注関連コード更新処理
	 * @event   : (i)画面情報
	 * @act_flg : (i)処理モード
	 * retsOrder : (o)双日様向けレコード更新結果
	 * retgOrder : (o)業者様向けレコード更新結果
	 * return   : void
	 */
	function put_order(event, act_flg, retsOrder, retgOrder){
		var record = event.record;
		var txt_customer_id = record['txt_customer_id']['value'];
		var app_id_s = kintone.app.getRelatedRecordsTargetAppId('rel_s_order'); // 双日様向け受注アプリID
		var app_id_g = kintone.app.getRelatedRecordsTargetAppId('rel_g_order'); // 業者様向け受注アプリID
		var s_get_query = '';
		var g_get_query = '';
		var s_get_fields = ['$id'];
		var g_get_fields = ['$id'];
		var txt_cancel = '';
		
		// 解約理由取得
		if(act_flg === DEF_MODE_MOVE){
			txt_cancel = DEF_DRP_ORDER_MOVE;
		}
		else {
			txt_cancel = DEF_DRP_ORDER_CANCEL;
		}
		
		//------------------------------
		// 双日様向け受注レコード(GET)
		//------------------------------
		s_get_query = 'lup_customer_id = \"' + record['txt_customer_id']['value'] + '\"';
		s_get_query += ' and num_payment = \"0\"';
		var s_get_body = {
			'app' : app_id_s,
			'query' : s_get_query,
			'fields' : s_get_fields
		};
		
		var retsGet = kintoneUtility.rest.getAllRecordsByQuery(s_get_body).then(function(s_get_resp) {
if(dbg_flg)console.log('s_get_resp:', s_get_resp);
			//------------------------------
			// 双日様向け受注レコード(PUT)
			//------------------------------
			var s_put_data = s_get_resp['records'];
			var s_put_len = s_put_data.length;
			var s_put_param = [];
			if(s_put_len !== 0){
				for(var s = 0; s < s_put_len; s++){
					var s_put_buf = {};
					s_put_buf['id'] = s_put_data[s]['$id']['value'];
					s_put_buf['record'] = {};
					s_put_buf['record']['chk_cancel'] = {'value':[DEF_CHK_ORDER_CANCEL]};
					s_put_buf['record']['date_cancel'] = {'value':moment().format('YYYY-MM-DD')};
					s_put_buf['record']['drp_cancel'] = {'value':txt_cancel};
					s_put_buf['record']['lup_customer_id'] = {'value':txt_customer_id};

					s_put_param.push(s_put_buf);
				}
			
                var s_put_body = {
                    'app' : app_id_s,
                    'records' : s_put_param
                };
			
                retsOrder['ret'] = kintoneUtility.rest.putAllRecords(s_put_body).then(function(s_put_resp) {
                }).catch(function(s_put_error) {
                    console.log(s_put_error);
                    alert('協力会社様向け受注アプリの更新に失敗しました。\n[' + s_put_error.message + ']');
                });
			}
			else {
                retsOrder['ret'] = null;
            }
		}).catch(function(s_get_error) {
			console.log(s_get_error);
			alert('協力会社様向け受注アプリの取得に失敗しました。\n[' + s_get_error.message + ']');
		});
		
		//------------------------------
		// 業者様向け受注レコード(GET)
		//------------------------------
		g_get_query = s_get_query;
		var g_get_body = {
			'app' : app_id_g,
			'query' : g_get_query,
			'fields' : g_get_fields
		};
		
		var retgGet = kintoneUtility.rest.getAllRecordsByQuery(g_get_body).then(function(g_get_resp) {
if(dbg_flg)console.log('g_get_resp:', g_get_resp);
			//------------------------------
			// 業者様向け受注レコード(PUT)
			//------------------------------
			var g_put_data = g_get_resp['records'];
			var g_put_len = g_put_data.length;
			var g_put_param = [];
			if(g_put_len !== 0){
				for(var s = 0; s < g_put_len; s++){
					var g_put_buf = {};
					g_put_buf['id'] = g_put_data[s]['$id']['value'];
					g_put_buf['record'] = {};
					g_put_buf['record']['chk_cancel'] = {'value':[DEF_CHK_ORDER_CANCEL]};
					g_put_buf['record']['date_cancel'] = {'value':moment().format('YYYY-MM-DD')};
					g_put_buf['record']['drp_cancel'] = {'value':txt_cancel};
					g_put_buf['record']['lup_customer_id'] = {'value':txt_customer_id};
				
					g_put_param.push(g_put_buf);
				}
			
                var g_put_body = {
                    'app' : app_id_g,
                    'records' : g_put_param
                };
			
                retgOrder['ret'] = kintoneUtility.rest.putAllRecords(g_put_body).then(function(g_put_resp) {
                }).catch(function(g_put_error) {
                    console.log(g_put_error);
                    alert('業者様向け受注アプリの更新に失敗しました。\n[' + g_put_error.message + ']');
                });
			}
			else {
                retgOrder['ret'] = null;
            }
		}).catch(function(g_get_error) {
			console.log(g_get_error);
			alert('業者様向け受注アプリの取得に失敗しました。\n[' + g_get_error.message + ']');
		});
		

		return kintone.Promise.all([retsGet, retsOrder['ret'], retgGet, retgOrder['ret']]).then(function(results) {
		});
	}
	/* 
	 * function    : session取得および設定
	 * @event      : 画面情報
	 * return      : void
	 */
	function set_session_value(event){
		var record = event.record;
		var ses_val = null;
		
		//------------------------------
		// session情報取得
		//------------------------------
		if(window.sessionStorage.getItem(['record'])){
			ses_val = JSON.parse(window.sessionStorage.getItem(['record']));
		}
		// 設定なし
		else {
			// 終了
			return ;
		}
		
		//------------------------------
		// recordに設定
		//------------------------------
		// 物件情報
		record['txt_room_type']['value'] = ses_val.txt_room_type.value; // タイプ
		record['txt_room_color']['value'] = ses_val.txt_room_color.value; // カラー
		record['txt_room_layout']['value'] = ses_val.txt_room_layout.value; // 間取

		// 契約情報
		record['chk_nis_commit']['value'] = ses_val.chk_nis_commit.value; // Ni's確定
		record['chk_design_change']['value'] = ses_val.chk_design_change.value; // 設計変更
//		record['date_apply']['value'] = ses_val.date_apply.value; // 申込日付
//		record['date_contract']['value'] = ses_val.date_contract.value; // 契約日付
		record['date_cancel']['value'] = ses_val.date_cancel.value; // 解約日付
//		record['date_apply_free']['value'] = ses_val.date_apply_free.value; // 無償申込日
//		record['date_apply_charge']['value'] = ses_val.date_apply_charge.value; // 有償申込日
//		record['date_contract_check']['value'] = ses_val.date_contract_check.value; // 売契照合日
//		record['date_viewing']['value'] = ses_val.date_viewing.value; // 内覧日
//		record['date_check']['value'] = ses_val.date_check.value; // 確認会
//		record['date_transfer']['value'] = ses_val.date_transfer.value; // 引渡日
		record['drp_tax_class_calc']['value'] = ses_val.drp_tax_class_calc.value; // 税計算区分
		record['drp_tax_class']['value'] = ses_val.drp_tax_class.value; // 税区分
		record['drp_tax_calc']['value'] = ses_val.drp_tax_calc.value; // 税端数区分
		record['num_discount_price']['value'] = ses_val.num_discount_price.value; // 値引金額
		record['num_over_payment']['value'] = ses_val.num_over_payment.value; // 過入金

		// 契約者情報
		record['txt_contractor_name']['value'] = ses_val.txt_contractor_name.value; // 契約者名称
		record['txt_contractor_kana']['value'] = ses_val.txt_contractor_kana.value;// 得意先カナ名称
		record['txt_contractor_zip']['value'] = ses_val.txt_contractor_zip.value; // 契約者_郵便番号
		record['txt_contractor_add1']['value'] = ses_val.txt_contractor_add1.value; // 契約者_住所1
		record['txt_contractor_add2']['value'] = ses_val.txt_contractor_add2.value; // 契約者_住所2
		record['link_contractor_tel']['value'] = ses_val.link_contractor_tel.value; // 契約者_電話番号
		record['link_contractor_fax']['value'] = ses_val.link_contractor_fax.value; // 契約者_FAX番号
		record['mtxt_special_term']['value'] = ses_val.mtxt_special_term.value; // 特約条件

		// 連絡・送付先情報
		record['txt_contact_name']['value'] = ses_val.txt_contact_name.value; // 連絡先・送付先名称
		record['txt_contact_kana']['value'] = ses_val.txt_contact_kana.value; // 連絡先・送付先カナ名称
		record['txt_contact_zip']['value'] = ses_val.txt_contact_zip.value; // 連絡先_郵便番号
		record['txt_contact_add1']['value'] = ses_val.txt_contact_add1.value; // 連絡先_住所1
		record['txt_contact_add2']['value'] = ses_val.txt_contact_add2.value; // 連絡先_住所2
		record['link_contact_tel']['value'] = ses_val.link_contact_tel.value; // 連絡先_電話番号
		record['link_contact_fax']['value'] = ses_val.link_contact_fax.value; // 連絡先_FAX番号
		record['link_contact_phone']['value'] = ses_val.link_contact_phone.value; // 連絡先_携帯電話番号
		record['link_contact_mail']['value'] = ses_val.link_contact_mail.value; // 連絡先_Email
		
		// 前回情報
		record['txt_pre_build_cd']['value'] = ses_val.txt_build_cd.value; // 前回建物区分
		record['txt_pre_room_number']['value'] = ses_val.txt_room_number.value; // 前回住戸番号
		
		// 解約情報:複製なし

		// メモ情報
		record['mtxt_nis_memo']['value'] = ses_val.mtxt_nis_memo.value; // NIS担当者メモ
		record['mtxt_op_memo']['value'] = ses_val.mtxt_op_memo.value; // OP担当者メモ
		record['mtxt_demand_memo']['value'] = ses_val.mtxt_demand_memo.value; // 請求備考
		record['mtxt_payment_memo']['value'] = ses_val.mtxt_payment_memo.value; // 入金備考
		record['mtxt_etc_memo']['value'] = ses_val.mtxt_etc_memo.value; // その他備考
		record['mtxt_special_memo']['value'] = ses_val.mtxt_special_memo.value; // 特記事項
		
		// システム管理用
		record['txt_condo_cd']['value'] = ses_val.txt_condo_cd.value; // 物件CD
		record['lup_condo_cd']['value'] = ses_val.lup_condo_cd.value; // 物件CD
		record['txt_build_cd']['value'] = ses_val.txt_build_cd.value; // 建物CD
		record['txt_build_id']['value'] = ses_val.txt_build_id.value; // 建物ID
		record['lup_build_id']['value'] = ses_val.lup_build_id.value; // 建物ID
		record['txt_build_room']['value'] = ses_val.txt_build_room.value; // 建物ID+部屋番号

		//------------------------------
		// session clear
		//------------------------------
		window.sessionStorage.clear();

	}
	
	/* function  : フィールド表示/非表示
	 * @show_flg : true/表示 false/非表示
	 * return    : void
	 */
	function set_fieldshown(show_flg){
		kintone.app.record.setFieldShown('grp_admin', show_flg);
	}
	
	/* function  : 関連レコードチェック
	 * @event    : 画面情報
	 * return    : promise
	 */
	function chk_relate(event){
		return kintone.Promise(function(resolve, reject) {
			resolve(event);
		});
	}
	
	/* function : キャンセル/住戸移動処理
	 * @event   : 画面情報
	 * @act_flg : 処理モード
	 * return   : promise
	 */
	function put_cancel(event, act_flg){
        var record = event.record;
        var app_id = kintone.app.getId();
        var txt_cancel = '';
        
        // 解約/住戸移動フラグ確認
        if(!ex_change_number_flg){
            return ;
        }
        else {
            ex_change_number_flg = 0; // フラグクリア
        }
        
        // 処理モード確認
        if(act_flg === DEF_MODE_CANCEL){
            txt_cancel = DEF_DRP_CANCEL;
        }
        else {
            txt_cancel = DEF_DRP_MOVE;
        }
        
        //--------------------------------------
        // 申込みレコード更新(PUT)
        //--------------------------------------
        // 未入金かつ同一顧客IDをもつレコードを更新
        var retsOrder = {};
        var retgOrder = {};
        put_order(event, act_flg, retsOrder, retgOrder);
            
        //--------------------------------------
        // 住戸移動：新規レコード作成
        //--------------------------------------
        return kintone.Promise.all([retsOrder['ret'], retgOrder['ret']]).then(function(results) {
            if(act_flg === DEF_MODE_MOVE){
                window.sessionStorage.setItem(['record'],[JSON.stringify(record)]);
                var hostname = window.location.hostname ;
                var href_url = "https://" + hostname + "/k/" + app_id + "/edit";
                location.href= href_url;
            }
            // キャンセルのみ
            else {
                location.reload(true);
            }
        });
	}

	/* function : ボタン表示(物件選択)
	 * @event   : 画面情報
	 */
	function createModal(event){
		//ボタンを生成
		var spaceFieldButton = document.createElement('button');
		spaceFieldButton.id = 'buttonE';
		spaceFieldButton.setAttribute('class', 'kintoneplugin-button-normal');
		spaceFieldButton.innerHTML = '物件を選択';
		spaceFieldButton.onclick = function () {
			show_modal(event);
		};
		kintone.app.record.getSpaceElement('sp_btn_condo').appendChild(spaceFieldButton);
	}
	
	/* function : モーダル表示(物件選択)
	 * @event   : 画面情報
	 */
	function show_modal(event){
		var record = event.record;
		var data_list = {}; // 検索結果
		
		//------------------------------------------------------
		// modalを生成
		//------------------------------------------------------
		var put_html = '';
		var list_html = {};
		var el_sp = kintone.app.record.getSpaceElement('sp_modal_condo');
	
		put_html += '<div class="modal fade" id="specificModal" tabindex="-1">\n';
		put_html += '	<div class="modal-dialog" style="width:900px;max-width:100%;background-color:rgba(255,255,255,0);">\n';
		put_html += '		<div class="modal-content">\n';
	
		// put modal header
		put_html += '			<div class="modal-header">\n';
		put_html += '				<button type="button" class="close" id="cancel"><span>×</span></button>\n';
		put_html += '				<div class="modal-title h4">物件/建物選択</div>\n';
		put_html += '			</div>\n';
	
		// put modal body
		put_html += '			<div class="modal-body" style="max-height:100% px;overflow:auto;">\n';
		put_html += '				<table id="stbl_query" style="width:800px">\n';
		put_html += '				  <thead id="stbl_head">\n';
		put_html += '					<tr hight="30">\n';
		put_html += '						<td colspan="4">';
		put_html += '							物件コード<br/><input type="text" class="form-control" id="searchCode" placeholder="物件コードを入力" autofocus>\n';
		put_html += '							物件名<br/><input type="text" class="form-control" id="searchText" placeholder="物件名を入力" autofocus>\n';
		put_html += '						</td>';
		put_html += '					</tr>\n';
		put_html += '						<td valign="bottom">';
		put_html += '							<button type="button" class="btn btn-default" id="searchButton">検索</button>\n';
		put_html += '						</td>';
		put_html += '					</tr>\n';
		put_html += '				  </thead>\n';
		put_html += '				</table>\n';
		put_html += '				<table id="stbl_rec" style="width:800px">\n';
		put_html += '				  <tbody id="stbl_body">\n';
		// ：検索後、出力
        // html生成
        var retData = mk_list(event, [], list_html, data_list);
        kintone.Promise.all([retData]).then(function(results) {
            $('#stbl_body').find("tr").remove();
            $('#stbl_body').append(list_html['html']);
        });

		put_html += '				  </tbody>\n';
		put_html += '				</table>\n';
		put_html += '			</div>\n';
	
		// put modal footer
		put_html += '			<div class="modal-footer">\n';
		put_html += '				<button type="button" class="btn btn-default" id="cancelButton">キャンセル</button>\n';
		put_html += '				<button type="button" class="btn btn-primary" id="clearButton">クリア</button>\n';
		put_html += '			</div>\n';
		put_html += '		</div>\n';
		put_html += '	</div>\n';
		put_html += '</div>\n';
	
		// modal出力
		$(".modal-backdrop").remove();
		el_sp.innerHTML = put_html;
		
		// 検索ボタン押下処理 add by fukui
		$("#searchButton").click(function() {
            // 検索条件取得
            var stext = $('#searchText').val();
            var scode = $('#searchCode').val();
            var res_html = {};
            var skeys = [];
            skeys['stext'] = stext;
            skeys['scode'] = scode;

            if(!stext && !scode){
                alert('物件名または物件コードを指定してください。');
                return ;
            }
		
            // html生成
            var retData = mk_list(event, skeys, res_html, data_list);
            kintone.Promise.all([retData]).then(function(results) {
                $('#stbl_body').find("tr").remove();
                $('#stbl_body').append(res_html['html']);
            });
		});
		
		// 選択ボタン押下処理
		$(document).on('click', '.sel_value', function() {
			var ps = $(this).val();
			var put_data = data_list['data']['records'][ps];
			var get_rec = kintone.app.record.get();
			get_rec.record['txt_condo_cd']['value'] = put_data['lup_condo_cd']['value'];
			get_rec.record['txt_build_cd']['value'] = put_data['txt_build_cd']['value'];
			get_rec.record['txt_build_id']['value'] = put_data['txt_build_id']['value'];
			get_rec.record['lup_condo_cd']['value'] = put_data['lup_condo_cd']['value'];
			get_rec.record['lup_build_id']['value'] = put_data['txt_build_id']['value'];
			get_rec.record['lup_build_id']['lookup'] = true;
			get_rec.record['lup_condo_cd']['lookup'] = true;
			kintone.app.record.set(get_rec);
			$('#specificModal').modal('hide');
			el_sp.innerHTML = "";
		});
		
		// ×ボタン押下処理
		$("#cancel").click(function() {
			$('#specificModal').modal('hide');
			el_sp.innerHTML = "";
		});
		
		// キャンセルボタン押下処理
		$("#cancelButton").click(function() {
			$('#specificModal').modal('hide');
			el_sp.innerHTML = "";
		});
	
		// クリアボタン押下処理
		$("#clearButton").click(function() {
			$('#searchText').val('');
			$('#stbl_body').find("tr").remove();
			$("#searchText").focus();
		});
	
		// modal window表示処理
		$('#specificModal').modal({
			backdrop: "static"
		});
	}
	
	/*
	 * funciton    : 検索結果一覧html生成
	 * @event      : (i)画面情報
	 * @query_list : (i)検索条件 array
	 * @out_html   : (o)出力html
	 * @get_data   : (o)取得情報
	 * return      : promise
	 */
	function mk_list(event, query_list, out_html, get_data){
		var app_id = kintone.app.getRelatedRecordsTargetAppId('rel_build'); // 物件別建物アプリID
		var get_fields = ['lup_condo_cd', 'txt_condo_name', 'txt_build_cd', 'txt_build_name', 'txt_build_id'];
		var put_html = '';
		var get_query = '';
        if(query_list['stext']){
            get_query += 'txt_condo_name like \"' + query_list['stext'] + '\"';
        }
        if(query_list['scode']){
            if(get_query !== ''){
                get_query += ' or';
            }
            get_query += ' txt_condo_cd = \"' + query_list['scode'] + '\"';
        }
		get_query += ' order by lup_condo_cd limit 30';
//		var get_query = 'txt_condo_name like \"' + query_list + '\"';
//		get_query += ' order by lup_condo_cd ';
		
		var param = {
			"app" : app_id,
			"query" : get_query,
			"fields" : get_fields
		};

		return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', param).then(function(resp) {
			// 検索結果一覧html
			var view_item_code = ['txt_condo_name', 'txt_build_name'];
			var view_item_list = ['物件名', '建物名'];
			var v_cnt = view_item_list.length;
			var rec_cnt = resp['records']['length'];
			
			if(rec_cnt === 0){
				put_html += '					<tr hight="30">\n';
				put_html += '						<td>検索結果は0件です</td>\n';
				put_html += '					</tr>\n';
			}
			else {
				// 見出し行
				put_html += '					<tr hight="30">\n';
				put_html += '						<td><br/></td>\n';
				for(var j = 0; j < v_cnt; j++){  // 横：列数
					var v_name = view_item_list[j];
					put_html += '						<td>' + v_name + '</td>\n';
				}
				put_html += '					</tr>\n';
				for(var i = 0; i < rec_cnt; i++){ // 縦：行数
					var rec = resp['records'][i];
					// 選択行
					put_html += '					<tr hight="30">\n';
					put_html += '						<td>';
					put_html +=							 '<button type="button" value="' + i + '" class="btn btn-default sel_value">選択</button>';
					put_html +=						 '</td>\n';
					for(var j = 0; j < v_cnt; j++){  // 横：列数
						var v_field = view_item_code[j];
						put_html += '						<td>';
						put_html += rec[v_field]['value'];
						put_html +=						 '</td>\n';
					}
					put_html += '					</tr>\n';
				}
			}
			out_html['html'] = put_html;
			get_data['data'] = resp;
		}, function(error) {
			// error
			alert("物件別建物アプリの検索に失敗しました。\n[" + error.message + "]\n");
			console.log(error);
		});
	}
	
	/*
	 * function : ドロップダウン表示
	 * @event   : (i)画面情報
	 */
	function put_drplist(event){
		var record = event.record;
		var txt_condo_cd = record['txt_condo_cd']['value']; // 物件CD

		// init
        $('.drp_list').remove();

		//-------------------------------------------
		// html生成
		//-------------------------------------------
		// プラン管理マスタからGET
		var get_query =  'txt_condo_cd = \"' + txt_condo_cd + '\"';
		get_query += ' and chk_cancel not in (\"' + DEF_CHK_CANCEL + '\")';
		get_query += ' order by txt_room_type asc';
		var get_fields = ['$id', 'txt_room_type', 'txt_room_color', 'txt_room_layout'];
	
		var get_param = {
			app : kintone.app.getId(),
			query : get_query,
			fields : get_fields
		};
		
		kintoneUtility.rest.getAllRecordsByQuery(get_param).then(function(get_resp) {
			var data_arr = [];
			var rec_len = get_resp.records.length;
			
			// sql用データリスト生成
			for(var j = 0; j < rec_len; j++){
				var data_buf = {};
				data_buf['type'] = get_resp.records[j]['txt_room_type']['value'];
				data_buf['color'] = get_resp.records[j]['txt_room_color']['value'];
				data_buf['layout'] = get_resp.records[j]['txt_room_layout']['value'];
				data_arr.push(data_buf);
			}
			
			// 出力情報設定
			var put_info = [];
			put_info.push({'id':'drp_typeList', 'field_cd':'txt_room_type', 'column':'type', 'sp':'sp_room_type'});
			put_info.push({'id':'drp_colorList', 'field_cd':'txt_room_color', 'column':'color', 'sp':'sp_room_color'});
			put_info.push({'id':'drp_layoutList', 'field_cd':'txt_room_layout', 'column':'layout', 'sp':'sp_room_layout'});

			// html生成
			for(var k = 0; k < put_info.length; k++){
				var put_html = '';
				var info_param = put_info[k];
				
				// 出力データ生成(sql実行)
				var str_sql = '';
				str_sql += 'SELECT DISTINCT ' + info_param['column'] + ' FROM ? AS d_list ORDER BY ' + info_param['column'];
				var resp_data = alasql(str_sql, [data_arr]);
				var resp_len = resp_data.length;
if(dbg_flg)console.log(resp_data);

				// ドロップリスト生成
				put_html += '<div class=\"kintoneplugin-select-outer\">\n';
				put_html += '    <div class=\"kintoneplugin-select\">\n';
				put_html += '        <select id=\"' + info_param['id'] + '\">\n';
				put_html += '            <option value=\"\">------------</option>\n';
				for(var i = 0; i < resp_len; i++){
					var pname = info_param['column'];
					var txt_column = resp_data[i][pname];
					put_html += '            <option value=\"' + i + '\">' + txt_column + '</option>\n';
				}
				put_html += '        </select>\n';
				put_html += '    </div>\n';
				put_html += '</div>\n';
				
				// 画面出力
				var el_div = document.createElement('div');
				el_div.setAttribute('class', 'drp_list'); // add class for CSS by fukui
				el_div.innerHTML = put_html;
				kintone.app.record.getSpaceElement(info_param['sp']).appendChild(el_div);
			}
			
			//-------------------------------------------
			// 選択時制御
			//-------------------------------------------
			// タイプ
			$('#drp_typeList').on('change', function() {
				var sel_txt_type = $("#drp_typeList option:selected").text();
				// レコード情報取得
				var get_rec = kintone.app.record.get();
				get_rec.record['txt_room_type']['value'] = sel_txt_type;
				kintone.app.record.set(get_rec);
			});
			// カラー
			$('#drp_colorList').on('change', function() {
				var sel_txt_color = $("#drp_colorList option:selected").text();
				// レコード情報取得
				var get_rec = kintone.app.record.get();
				get_rec.record['txt_room_color']['value'] = sel_txt_color;
				kintone.app.record.set(get_rec);
			});
			// 間取り
			$('#drp_layoutList').on('change', function() {
				var sel_txt_layout = $("#drp_layoutList option:selected").text();
				// レコード情報取得
				var get_rec = kintone.app.record.get();
				get_rec.record['txt_room_layout']['value'] = sel_txt_layout;
				kintone.app.record.set(get_rec);
			});
			
		}).catch(function(get_error) {
			console.log(get_error);
			alert('自アプリの取得に失敗しました。\n[' + get_error.message + ']');
		});
		
		return event;
	}

	//--------------------------------------
	// 解約時制御
	//--------------------------------------
	kintone.events.on(EVENT_CHANGE_CHK_CANCEL, function(event) {
		//--------------------------------------
		// 自動設定
		//--------------------------------------
		ctrl_cancel(event);
		
		return event;
	});
	
	//--------------------------------------
	// 住戸移動時制御
	//--------------------------------------
	kintone.events.on(EVENT_CHANGE_CHK_MOVE, function(event) {
		//--------------------------------------
		// 自動設定
		//--------------------------------------
		ctrl_move(event);
		
		return event;
	});
	
	//--------------------------------------
	// 物件コード変更時制御
	//--------------------------------------
	kintone.events.on(EVENT_CHANGE_CONDO_CD, function(event) {
		//--------------------------------------
		// ドロップダウン出力
		//--------------------------------------
		put_drplist(event);
		
	});
	
	//--------------------------------------
	// 保存完了後処理
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SUCCESS, function(event) {
	    var record = event.record;
	    
		//--------------------------------------
		// 関連アプリ更新処理
		//--------------------------------------
		updapp_customer(event, kintone.app.getRelatedRecordsTargetAppId('rel_s_order')); // 協力会社様向け受注管理
		updapp_customer(event, kintone.app.getRelatedRecordsTargetAppId('rel_g_order')); // 業者様向け受注管理
		
		//--------------------------------------
		// 解約/住戸移動時更新処理
		//--------------------------------------
		var act_flg = 0;
		if(record['chk_cancel']['value'].length !== 0){
            act_flg = DEF_MODE_CANCEL;
		}
		else if(record['chk_move']['value'].length !== 0){
            act_flg = DEF_MODE_MOVE;
        }
        if(act_flg !== 0){
            put_cancel(event, act_flg);
        }	
	});
	
	//--------------------------------------
	// 保存ボタン押下時制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SUBMIT, function(event) {
        var record = event.record;
	    
		//--------------------------------------
		// 自動設定
		//--------------------------------------
		var txt_build_id = record['txt_build_id']['value']; // 建物ID
		var txt_room_number = record['txt_room_number']['value']; // 部屋番号
		var txt_build_room = txt_build_id + '_' + txt_room_number;
		
		record['txt_build_room']['value'] = txt_build_room; // 建物ID+部屋番号
		
		// 必須チェック
		chk_required(event);
		
		// 重複チェック
		var retUnique = chk_unique(event);
		
		// 解約・住戸移動時
		var retNum = set_cancel_move(event);
		
		// 物件別業者マスタ取得および設定
		var retItem = get_itemlist(event);
		
		return kintone.Promise.all([retUnique, retNum, retItem]).then(function(results) {
            return event;
		});
		
	});
	
	//--------------------------------------
	// 詳細画面制御
	//--------------------------------------
	kintone.events.on(EVENT_DETAIL_SHOW, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
        var record = event.record;
        
		//--------------------------------------
		// 表示/非表示
		//--------------------------------------
		set_fieldshown(false);

        // 入金総額表示
        calc_receipt(event);
        
		return event;
		
	});
	
	//--------------------------------------
	// 一覧画面制御
	//--------------------------------------
//	kintone.events.on(EVENT_INDEX_SHOW, function(event) {
//		return event;
//	});
	
	//--------------------------------------
	// 登録/編集画面制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SHOW, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
		var record = event.record;
		
		//--------------------------------------
		// 初期値設定/複製対応
		//--------------------------------------
		if(event.type === 'app.record.create.show'){
            // Session取得および設定
            set_session_value(event);

            // 物件選択用モーダル出力
            createModal(event);

            // 初期値
            record['chk_cancel']['value'].length = 0;
            record['drp_cancel']['value'] = ''; // 解約理由：選択不可
            record['mtxt_cancel_note']['value'] = ''; // 解約理由：編集可
            record['date_cancel']['value'] = ''; // 解約日付：編集不可
            record['chk_move']['value'].length = 0;
            record['date_move']['value'] = '';	    
            record['mtxt_move_note']['value'] = ''; // 住戸移動理由：編集可
		}
		
		//--------------------------------------
		// 表示/非表示
		//--------------------------------------
		set_fieldshown(false);
		
		//--------------------------------------
		// 編集可/不可
		//--------------------------------------
		if(event.type !== 'app.record.create.show'){
            record['txt_condo_cd']['disabled'] = true; // 物件CD
            record['txt_build_cd']['disabled'] = true; // 建物CD
            record['txt_build_id']['disabled'] = true; // 建物ID
            record['txt_room_number']['disabled'] = true; // 部屋番号
		}

		// 解約チェック済
		if(record['chk_cancel']['value'].length !== 0){
            record['chk_cancel']['disabled'] = true;	    
            record['chk_move']['disabled'] = true;
            record['drp_cancel']['disabled'] = false; // 解約理由：選択不可
            record['mtxt_cancel_note']['disabled'] = false; // 解約理由：編集可
		}
		else {
            record['drp_cancel']['disabled'] = true; // 解約理由：選択不可
            record['date_cancel']['disabled'] = true; // 解約日付：編集不可
            record['mtxt_cancel_note']['disabled'] = true; // 解約理由：編集不可
		}
		// 住戸移動チェック済
		if(record['chk_move']['value'].length !== 0){
            record['chk_cancel']['disabled'] = true;	    
            record['chk_move']['disabled'] = true;	    
            record['mtxt_move_note']['disabled'] = false; // 住戸移動理由：編集可
		}
		else {
            record['mtxt_cancel_note']['disabled'] = true; // 解約理由：編集不可
            record['date_move']['disabled'] = true; // 住戸移動日付：編集不可
            record['mtxt_move_note']['disabled'] = true; // 住戸移動理由：編集不可
		}
		
		//--------------------------------------
		// ドロップダウン出力
		//--------------------------------------
		put_drplist(event);
		
		// 入金総額表示
		calc_receipt(event);
		
		//--------------------------------------
		// 住所郵便番号→住所変換ボタン
		//--------------------------------------
		var btn_arr = [
			'sp_contractor_zip',
			'sp_contact_zip'
		];
		
		for(var i = 0; i < btn_arr.length; i++){
			var button = document.createElement('button');
			button.className = 'kintoneplugin-button-normal';//プラグイン向けCSSを流用する場合
			button.id = btn_arr[i];
			button.innerHTML = '住所を取得';
			button.style = 'margin-top: 29px';
			button.onclick = function() {
				var btn_id = $(this).attr('id');
				set_address(event, btn_id);
			};
			kintone.app.record.getSpaceElement(btn_arr[i]).appendChild(button);
		}

		//--------------------------------------
		// 住所コピーボタン
		//--------------------------------------
		var el_cp_btn = document.createElement('button');
		el_cp_btn.className = 'kintoneplugin-button-normal';//プラグイン向けCSSを流用する場合
		el_cp_btn.innerHTML = '契約者住所をコピー';
		el_cp_btn.style = 'margin-top: 29px';
		el_cp_btn.onclick = function() {
			copy_address(event);
		};
		kintone.app.record.getSpaceElement('sp_copy_btn').appendChild(el_cp_btn);

		return event;
		
	});
	
	//------------------------------------------------------
	// 一覧編集回避処理
	//------------------------------------------------------
	kintone.events.on(EVENT_INDEX_EDIT_SHOW, function(event) {
		//------------------------------
		// 変数宣言
		//------------------------------
		var record = event.record;
		
		//--------------------------
		// 一覧画面編集回避対応
		//--------------------------
		for(var str in record){
			if(event.record[str]){
				event.record[str]['disabled'] = true;
			}
		}
		
		return event;
	});

})(jQuery);
